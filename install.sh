#!/bin/bash

if [[ $1 != "controller" && $1 != "agent" ]]; then
	echo "Usage: install.sh <controller | agent>"
	echo "To install in controller, call from inside nic_test project"
	echo "To install in agent, call from the folder where to install LNST"
	exit 1
fi

# LNST binary dependencies
dnf install -y git gcc python3 python3-devel libnl3-devel libxml2 libxslt

# python 3.9 or higher is required (default in RHEL 8 is 3.6)
python_minor=$(python3 -c 'import sys; print(sys.version_info.minor)')
if (( $python_minor < 9 )); then
	dnf install -y python39 python39-devel || exit 1
	alternatives --set python3 /usr/bin/python3.9
fi

# poetry (install into /usr/local/bin which is in the PATH both in RHEL 8 and 9)
if [[ -z "$(command -v poetry)" ]]; then
	curl -sSL https://install.python-poetry.org | POETRY_HOME=/usr/local python3 -
fi

# in controller machine:
if [[ $1 == "controller" ]]; then
	# python modules dependencies (including LNST)
	poetry install
fi

# in agent machine:
if [[ $1 == "agent" ]]; then
	# standalone LNST and its python modules dependencies
	git clone https://github.com/LNST-project/lnst
	cd lnst
	poetry install

	# other required tools
	dnf install -y iperf3
fi
