Install (in controller machine)
===============================

From nic_test folder, run the installer script to install:
1. Binary dependencies of LNST
2. Poetry package manager for Python
3. Python modules dependencies of nic_test (including LNST itself)

```
./install.sh controller
```

> WARN: poetry is installed downloading and executing its own installer
> script from its website (it's not installed from our repos)

Install (in agent machines)
===========================

From the directory where to install LNST, run the installer script to install:
1. Binary dependencies of LNST
2. Poetry package manager for Python
3. Python modules dependencies of LNST
4. LNST

```
./install.sh agent
```

> WARN: poetry is installed downloading and executing its own installer
> script from its website (it's not installed from our repos)

Run
===========

First, create the machine's pools in the controller machine, directory
~/.lnst/pool/. The template template_machine.xml can be used.

Then, start lnst-agent processes in agent machines
```
systemctl stop NetworkManager
sysctl net.ipv6.conf.default.accept_ra=0  # see troubleshooting
cd lnst
poetry run lnst-agent
```

Then, start nic_test in controller machine
```
poetry run ./nic_test.py -d DRIVER

# for usage and help
poetry run ./nic_test.py --help
```

Troubleshooting
===============

Error 'Failed to create the collection: Prompt dismissed..'
----
Sometimes appears installing Poetry. Set environment variable: PYTHON_KEYRING_BACKEND="keyring.backends.null.Keyring". 

Source/destination ip lists are of different size
----
In some network environtments, IPv6 addresses can be obtained with
Router Advertisment messages. This triggers a bug in LNST, that detects that the
number of IP addresses is not what it expected, raising an error (in debug mode
it says 'Source/destination ip lists are of different size').

This can be solved executing `sysctl net.ipv6.conf.default.accept_ra=0` before
running the recipe.

See [https://github.com/LNST-project/lnst/issues/210]().
