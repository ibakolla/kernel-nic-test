#!/usr/bin/env python3

from lnst.Controller import Controller
from lnst.Recipes.ENRT import SimpleNetworkRecipe, PingFloodRecipe, \
    VlansRecipe, BondRecipe
from lnst.Controller.RunSummaryFormatter import RunSummaryFormatter
import argparse
import importlib
import logging
import traceback

default_recipes = {
    "SimpleNetwork": (SimpleNetworkRecipe, {
        "perf_tests": ["tcp_stream", "udp_stream"],
        "perf_msg_sizes": [1400]
    }),
    "PingFlood": (PingFloodRecipe, {
    }),
    "Vlans": (VlansRecipe, {
        "perf_tests": ["tcp_stream", "udp_stream"],
        "perf_msg_sizes": [1400],
        "vlan0_id": 1000,  # klab friendly VLANs
        "vlan1_id": 2000,
        "vlan2_id": 3000
    }),
    "Bond": (BondRecipe, {
        "perf_tests": ["tcp_stream", "udp_stream"],
        "perf_msg_sizes": [1400],
        "bonding_mode": "active-backup",
        "miimon_value": 250
    })
}

def main():
    args = parse_args()

    recipes_names = set(args.recipe if args.recipe else default_recipes.keys())
    recipes_names.difference_update(set(args.exclude))

    print("Recipes to run: {}". format(", ".join(recipes_names)))

    recipes_data = get_recipes_data(recipes_names)
    set_driver_arg(recipes_data, args.driver)
    set_recipes_custom_args(recipes_data, args.arg)

    recipes = create_recipes(recipes_data)

    run_recipes(recipes, debug=args.verbose)
    log_recipes_results(recipes)

def create_recipes(recipes_data):
    return [constructor(**args) for constructor, args in recipes_data.values()]

def run_recipes(recipes, debug=False):
    ctl = Controller(debug=debug)
    for recipe in recipes:
        try:
            ctl.run(recipe)
        except:
            traceback.print_exc()
            logging.error("error running recipe %s", recipe.__class__.__name__)

def log_recipes_results(recipes):
    summary_fmt = RunSummaryFormatter(colourize=True)
    for recipe in recipes:
        for run in recipe.runs:
            logging.info(summary_fmt.format_run(run))

def get_recipes_data(recipes_names):
    fails = []
    recipes_data = {}

    for recipe_name in recipes_names:
        try:
            if recipe_name in default_recipes:
                recipes_data[recipe_name] = default_recipes[recipe_name]
            else:
                recipes_data[recipe_name] = get_non_default_recipe_data(recipe_name)
        except Exception as e:
            fails.append(str(e))

    if fails:
        fails = "\n  ".join(fails)
        raise Exception(f"custom recipes import errors\n  {fails}")

    return recipes_data

def get_non_default_recipe_data(recipe_name):
    mod_name = recipe_name + "Recipe"
    class_name = mod_name
    args_name = recipe_name + "Args"

    try:
        mod = import_non_default_recipe_module(mod_name)
        recipe_class = getattr(mod, class_name)
        recipe_args = getattr(mod, args_name)
        return (recipe_class, recipe_args)
    except ModuleNotFoundError:
        raise Exception(f"module not found: {mod_name}")
    except AttributeError:
        raise Exception(f"module {mod_name} has to define {class_name} and {args_name}")

def import_non_default_recipe_module(mod_name):
    for prefix, pkg in [("", None), (".", "lnst.Recipes.ENRT")]:
        try:
            return importlib.import_module(prefix + mod_name, pkg)
        except ModuleNotFoundError:
            continue
    raise ModuleNotFoundError(mod_name)

def set_driver_arg(recipes_data, driver):
    for _, args in recipes_data.values():
        args["driver"] = driver

def set_recipes_custom_args(recipes_data, recipes_args):
    for key, val in recipes_args:
        recipe_name, arg = key.split(".")

        if recipe_name not in recipes_data or not arg:
            raise ValueError(f"Invalid --arg KEY: {key}")
        elif val[0] == "[" and val[-1] != "]":
            raise ValueError(f"Invalid --arg VAL: {val}")

        if val[0] == "[":
            val = [v.strip() for v in val[1:-1].split(",")]
            if all(map(str.isnumeric, val)):
                val = [int(v) for v in val]
        else:
            val = val.strip()
            if val.isnumeric():
                val = int(val)

        recipes_data[recipe_name][1][arg] = val

def parse_args():
    parser = argparse.ArgumentParser(prog="nic-test",
                    description="Run regression tests for NIC drivers using LNST",
                    epilog="Default recipes: {}".format(", ".join(default_recipes.keys())))

    parser.add_argument("-d", "--driver", required=True,
                        help="driver to select interfaces from the pool file")
    parser.add_argument("-v", "--verbose", action='store_true',
                        help="print LNST debug messages during the tests")
    parser.add_argument("-r", "--recipe", action="append",
                        help="run this recipe, more than one can be passed, " \
                            "or leave empty for defaults (values must be " \
                            "LNST class names without the 'Recipe' suffix)")
    parser.add_argument("-e", "--exclude", action="append", default=[],
                        metavar="RECIPE", help="exclude recipe of being run")
    parser.add_argument("-a", "--arg", nargs=2, action="append", default=[],
                        metavar=("KEY", "VAL"),
                        help="add/modify a recipe's default arg.\n"
                        "KEY format is recipe_name.arg_name.\n"
                        "For lists, VAL can be a comma separated list inside square brackets")

    return parser.parse_args()

if __name__ == "__main__":
    main()
